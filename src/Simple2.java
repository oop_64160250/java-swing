import javax.swing.JButton;
import javax.swing.JFrame;

class MyFrame extends JFrame {

    JButton button;

    public MyFrame() {
        super("first JFrame");
        button = new JButton("Click");
        button.setBounds(130, 100, 100, 40);
        this.add(button);
        this.setLayout(null);
        this.setSize(400, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        
    }

}
public class Simple2 {
    public static void main(String[] args) {
    MyFrame frame = new MyFrame();
    }
}
